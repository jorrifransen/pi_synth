
include "lib/basics"

function empty_func()
end

workspace "Synth"
    location "build"
    configurations { "Debug", "Release" }

    project "synth"
        kind "ConsoleApp"
        language "C++"
        location "build"
        targetdir "build"

        files { "include/synth/**.h", "source/**.cpp" }

        includedirs { "include/synth",  "lib/portaudio/include" }
        include_basics_headers("lib/basics")

        libdirs { "build/portaudio" }
        links { "pthread", "asound", "jack", "portaudio_static" }
        link_basics()

        filter "configurations:Debug"
            defines { "DEBUG" }
            symbols "On"

        filter "configurations:Release"
            defines { "NDEBUG" }
            optimize "On"

    use_basics_project("build/basics", "lib/basics", empty_func)
