#pragma once

enum Wave_Type
{
    WAVE_SINE,
    WAVE_SQUARE,
    WAVE_TRIANGLE,
    WAVE_SAW_AN,
    WAVE_SAW_DI,
    WAVE_NOISE,

    WAVE_COUNT,
};

static const char* Wave_Type_Names[WAVE_COUNT] =
{
    "WAVE_SINE",
    "WAVE_SQUARE",
    "WAVE_TRIANGLE",
    "WAVE_SAW_AN",
    "WAVE_SAW_DI",
    "WAVE_NOISE",
};

struct Oscillator
{
    Wave_Type wave_type;
    double frequency;
    double amplitude;
};

void oscillator_init(Oscillator* osc, double frequency, Wave_Type wave_type, double amplitude);
double oscillator_get_sample(Oscillator* osc, double time);

const char* get_wave_type_name(Wave_Type type);
