#pragma once

struct Envelope
{
    bool active;
    bool on;

    double amplitude;
    double trigger_off_amplitude;

    double attack_time;
    double decay_time;
    double sustain_amplitude;
    double release_time;

    double trigger_on_time;
    double trigger_off_time;
};

double envelope_get_amplitude(Envelope* env, double time);

void envelope_trigger_on(Envelope* env, double time);
void envelope_trigger_off(Envelope* env, double time);
