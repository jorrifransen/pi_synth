#pragma once

#include <pthread.h>

#define MIDI_MASK_STATUS_BYTE (0x80)

enum Midi_Event_Type
{
    MIDI_EVENT_NOTE,
    MIDI_EVENT_CONTROL_CHANGE,
    MIDI_EVENT_PROGRAM_CHANGE,
    MIDI_EVENT_PITCH_BLEND,
};

enum Midi_Note_Event_Action
{
    MIDI_NOTE_EVENT_ON,
    MIDI_NOTE_EVENT_OFF,
};

enum Midi_Status_Mask
{
    MIDI_STATUS_NOTE_OFF = 0x80,
    MIDI_STATUS_NOTE_ON = 0x90,
    MIDI_STATUS_POLY_AFTERTOUCH = 0xA0,
    MIDI_STATUS_CONTROL_CHANGE = 0xB0,
    MIDI_STATUS_PROGRAM_CHANGE = 0xC0,
    MIDI_STATUS_CHANNEL_AFTERTOUCH = 0xD0,
    MIDI_STATUS_PITCH_BEND = 0xE0,
};

struct Midi_Note_Event
{
    Midi_Note_Event_Action action;

    unsigned char number;
    unsigned char velocity;
};

struct Midi_Control_Change_Event
{
    unsigned char control;
    unsigned char value;
};

struct Midi_Program_Change_Event
{
    unsigned char number;
};

struct Midi_Pitch_Blend_Event
{
    short value;
};

struct Midi_Event
{
    Midi_Event_Type type;
    unsigned char channel;

    union
    {
        Midi_Note_Event note;
        Midi_Control_Change_Event control_change;
        Midi_Program_Change_Event program_change;
        Midi_Pitch_Blend_Event pitch_blend;
    };
};

typedef void (*F_On_Midi_Event)(const Midi_Event& event, void* user_data);

struct Midi_Device
{
    const char* name;
    int handle;

    unsigned char current_byte;
    unsigned char last_status;

    pthread_t thread;
    bool listening;

    F_On_Midi_Event event_callback;
    void* event_user_data;
};

void midi_dev_open(Midi_Device* m_dev, const char* name);
void midi_dev_close(Midi_Device* m_dev);
void midi_dev_start_listening(Midi_Device* m_dev);
void midi_dev_stop_listening(Midi_Device* m_dev);
void midi_dev_set_callback(Midi_Device* m_dev, F_On_Midi_Event callback, void* user_data);
void midi_event_print(const Midi_Event& event);

static void midi_dev_invoke_event_callback(Midi_Device* m_dev, const Midi_Event& event);

static void* midi_dev_parse_stream(void* _m_dev);
static void midi_parse_status(Midi_Device* m_dev);

static void midi_next_byte(Midi_Device* m_dev);
static unsigned char midi_dev_read_byte(Midi_Device* m_dev);
static bool midi_dev_bytes_available(Midi_Device* m_dev);

