#pragma once

#include "envelope.h"
#include "oscillator.h"

#include <basics/types.h>

#include <portaudio.h>

#include <stdio.h>

#define SAMPLE_RATE (44100)
#define FRAMES_PER_BUFFER (512)

#define NUM_OSCILLATORS (2)

struct Voice
{
    Envelope envelope;
    bool active;
    double start_time;

    float frequency;
    u8 note;
    u8 velocity;
};

struct Synth
{
    u16 num_voices;
    Voice* voices;

    double stream_time;
    PaStream* stream;

    Envelope prefab_envelope;

    Oscillator oscillators[NUM_OSCILLATORS];
    Oscillator* source_osc_1;
    Oscillator* source_osc_2;
};

void synth_init(Synth* synth, u16 num_voices);
void synth_deinit(Synth* synth);

void synth_note_down(Synth* synth, u8 note, u8 velocity);
void synth_note_up(Synth* synth, u8 note);

static Voice* synth_get_free_voice(Synth* synth);
