#include "envelope.h"

#include <cassert>
#include <stdio.h>

double envelope_get_amplitude(Envelope* env, double time)
{
    if (env->on)
    {
        double time_active = time - env->trigger_on_time;
        if (time_active <= env->attack_time)
        {
            // Attack
            if (env->attack_time == 0.0)
            {
                env->amplitude = 1.0;
            }
            else
            {
                env->amplitude = time_active / env->attack_time;
            }
        }

        if (time_active > env->attack_time && time_active <= env->attack_time + env->decay_time)
        {
            // Decay
            if (env->decay_time == 0.0)
            {
                env->amplitude = env->sustain_amplitude;
            }
            else
            {
                double decay_progress = (time_active - env->attack_time) / env->decay_time;
                env->amplitude = 1.0 - ((1.0 - env->sustain_amplitude) * decay_progress);
            }
        }

        if (time_active > (env->attack_time + env->decay_time))
        {
            // Sustain
            env->amplitude = env->sustain_amplitude;
        }

    }
    else
    {
        double time_inactive = time - env->trigger_off_time;
        if (time_inactive <= env->release_time)
        {
            // Release
            if (env->release_time == 0.0)
            {
                env->amplitude = 0.0;
            }
            else
            {
                double release_progress = time_inactive / env->release_time;
                env->amplitude = env->trigger_off_amplitude -
                    (env->trigger_off_amplitude * release_progress);
            }
        }
        else
        {
            env->active = false;
        }

    }

    return env->amplitude;
}

void envelope_trigger_on(Envelope* env, double time)
{
    assert(env);

    env->active = true;
    env->on = true;
    env->trigger_on_time = time;
    env->amplitude = 0.0;
}

void envelope_trigger_off(Envelope* env, double time)
{
    assert(env);

    env->on = false;
    env->trigger_off_time = time;
    env->trigger_off_amplitude = env->amplitude;
}
