#include "midi.h"

#include <cassert>

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include <sys/select.h>

void midi_dev_open(Midi_Device* m_dev, const char* name)
{
    assert(m_dev);
    assert(name);

    *m_dev = {};
    m_dev->handle = open(name, O_RDONLY);
    if (m_dev->handle == -1)
    {
        fprintf(stderr, "Failed to open midi device: %s\n", name);
        assert(false);
    }

    m_dev->name = name;
}

void midi_dev_close(Midi_Device* m_dev)
{
    assert(m_dev);

    close(m_dev->handle);
}

void midi_dev_start_listening(Midi_Device* m_dev)
{
    assert(m_dev);

    m_dev->listening = true;
    pthread_create(&m_dev->thread, nullptr, midi_dev_parse_stream, m_dev);
}

void midi_dev_stop_listening(Midi_Device* m_dev)
{
    assert(m_dev);

    m_dev->listening = false;
    pthread_join(m_dev->thread, nullptr);
}

void midi_dev_set_callback(Midi_Device* m_dev, F_On_Midi_Event callback, void* user_data)
{
    assert(m_dev);


    m_dev->event_callback = callback;
    m_dev->event_user_data = user_data;
}

void midi_event_print(const Midi_Event& event)
{
    switch (event.type)
    {
        case MIDI_EVENT_NOTE:
            printf("Note event:\n");
            if (event.note.action == MIDI_NOTE_EVENT_ON)
            {
                printf("\tAction: on\n");
            }
            else
            {
                printf("\tAction: off\n");
            }
            printf("\tNote: %d\n", event.note.number);
            printf("\tVelocity: %d\n", event.note.velocity);
            printf("\tChannel: %d\n", event.channel);
            break;

        case MIDI_EVENT_CONTROL_CHANGE:
            printf("Control change event\n");
            printf("\tcontrol: %d\n", event.control_change.control);
            printf("\tvalue: %d\n", event.control_change.value);
            printf("\tChannel: %d\n", event.channel);
            break;

        case MIDI_EVENT_PROGRAM_CHANGE:
            printf("Program change event\n");
            printf("\tnumber: %d\n", event.program_change.number);
            printf("\tChannel: %d\n", event.channel);
            break;

        case MIDI_EVENT_PITCH_BLEND:
            printf("Pitch blend event\n");
            printf("\tValue: %d\n", event.pitch_blend.value);
            printf("\tChannel: %d\n", event.channel);
            break;
    }

}

static void midi_dev_invoke_event_callback(Midi_Device* m_dev, const Midi_Event& event)
{
    assert(m_dev);

    if (m_dev->event_callback != nullptr)
    {
        m_dev->event_callback(event, m_dev->event_user_data);
    }
}

static void* midi_dev_parse_stream(void* _m_dev)
{
    assert(_m_dev);
    auto m_dev = (Midi_Device*)_m_dev;

    printf("Start listening\n");

    while (m_dev->listening)
    {
        if (midi_dev_bytes_available(m_dev) == false)
        {
            sleep(0.0001f);
            continue;
        }

        midi_next_byte(m_dev);

        if (m_dev->current_byte | MIDI_MASK_STATUS_BYTE)
        {
            m_dev->last_status = m_dev->current_byte;
            midi_next_byte(m_dev);
        }

        midi_parse_status(m_dev);
    }

    return 0;
}

static void midi_parse_status(Midi_Device* m_dev)
{
    assert(m_dev);

    unsigned char status_byte = m_dev->last_status;

    Midi_Event event = {};
    event.channel = status_byte & 0x0F;

    switch (status_byte & 0xF0)
    {
        case MIDI_STATUS_NOTE_ON:
            event.type = MIDI_EVENT_NOTE;
            event.note.number = m_dev->current_byte;
            midi_next_byte(m_dev);
            event.note.velocity = m_dev->current_byte;
            if (event.note.velocity == 0)
            {
                event.note.action = MIDI_NOTE_EVENT_OFF;
            }
            else
            {
                event.note.action = MIDI_NOTE_EVENT_ON;
            }

            break;

        case MIDI_STATUS_NOTE_OFF:
            event.type = MIDI_EVENT_NOTE;
            event.note.action = MIDI_NOTE_EVENT_OFF;
            event.note.number = m_dev->current_byte;
            midi_next_byte(m_dev);
            event.note.velocity = m_dev->current_byte;

            break;

        case MIDI_STATUS_CONTROL_CHANGE:
            event.type = MIDI_EVENT_CONTROL_CHANGE;
            event.control_change.control = m_dev->current_byte;
            midi_next_byte(m_dev);
            event.control_change.value = m_dev->current_byte;
            break;

        case MIDI_STATUS_PROGRAM_CHANGE:
            event.type = MIDI_EVENT_PROGRAM_CHANGE;
            event.program_change.number = m_dev->current_byte;
            break;

        case MIDI_STATUS_PITCH_BEND:
        {
            event.type = MIDI_EVENT_PITCH_BLEND;
            unsigned short ls_byte = m_dev->current_byte;
            midi_next_byte(m_dev);
            unsigned short ms_byte = m_dev->current_byte << 7;
            event.pitch_blend.value = ls_byte + ms_byte;
            break;
        }

        default:
            assert(false);
            break;
    }

    midi_dev_invoke_event_callback(m_dev, event);
}

static void midi_next_byte(Midi_Device* m_dev)
{
    assert(m_dev);

    m_dev->current_byte = midi_dev_read_byte(m_dev);
}

static unsigned char midi_dev_read_byte(Midi_Device* m_dev)
{
    assert(m_dev);

    unsigned char result;

    read(m_dev->handle, &result, sizeof(result));

    return result;
}

static bool midi_dev_bytes_available(Midi_Device* m_dev)
{
    assert(m_dev);

    fd_set set;
    FD_ZERO(&set);
    FD_SET(m_dev->handle, &set);

    timeval timeout = {};
    int result = select(m_dev->handle + 1, &set, nullptr, nullptr, &timeout);

    if (result < 0)
    {
        assert(false);
    }
    else if (result == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

