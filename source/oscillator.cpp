#include "oscillator.h"

#include <cassert>
#include <math.h>

void oscillator_init(Oscillator* osc, double frequency, Wave_Type wave_type, double amplitude)
{
    assert(osc);

    osc->frequency = frequency;
    osc->wave_type = wave_type;
    osc->amplitude = amplitude;
}

double oscillator_get_sample(Oscillator* osc, double time)
{
    assert(osc);

    double result = 0.0;

    switch (osc->wave_type)
    {
        case WAVE_SINE:
            result = sin(osc->frequency * 2 * M_PI * time);
            break;

        case WAVE_SQUARE:
        {
            double s = sin(osc->frequency * 2 * M_PI * time);
            if (s > 0.0)
                s = 1.0;
            else
                s = -1.0;

            result = s;
        }
        break;

        case WAVE_TRIANGLE:
        {
            double s = sin(osc->frequency * 2 * M_PI * time);
            s = asin(s) * 2.0 / M_PI;
            result = s;
        }
        break;

        case WAVE_SAW_AN:
        {
            double s = 0.0;

            for (double i = 1.0; i < 100.0; i++)
            {
                s += (sin(i * osc->frequency * 2 * M_PI * time)) / i;
            }

            result = s * (2.0 / M_PI);
        }
        break;

        case WAVE_SAW_DI:
        {
            result = (2.0 / M_PI) *
                (osc->frequency * M_PI * fmod(time, 1.0 / osc->frequency) - (M_PI / 2.0));
        }
        break;

        case WAVE_NOISE:
        {
            result = 2.0 * ((double)rand() / (double)RAND_MAX) - 1.0;
        }
        break;

        default:
            assert(false);
            break;
    }

    return result * osc->amplitude;
};

const char* get_wave_type_name(Wave_Type type)
{
    assert(type < WAVE_COUNT);

    return Wave_Type_Names[type];
}
