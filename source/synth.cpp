#include "synth.h"

#include "midi_note_frequencies.h"

#include <cassert>

#include <basics/memory.h>

#include <stdio.h>

static int pa_callback(const void* input_buffer, void* output_buffer,
                       unsigned long frames_per_buffer,
                       const PaStreamCallbackTimeInfo* time_info,
                       PaStreamCallbackFlags status_flags,
                       void* user_data)
{
    Synth* synth = (Synth*)user_data;
    float* out = (float*)output_buffer;

    double sample_time = synth->stream_time;
    double sample_increment = 1.0 / (double)SAMPLE_RATE;
    for (unsigned long i = 0; i < frames_per_buffer; i++)
    {
        float s = 0.0f;

        int active_voices = 0;

        for (u16 i = 0; i < synth->num_voices; i++)
        {
            Voice* voice = synth->voices + i;
            if (voice->envelope.active)
            {
                active_voices++;

                synth->source_osc_1->frequency = voice->frequency;
                float _s = oscillator_get_sample(synth->source_osc_1, sample_time);
                _s *= (voice->velocity / 127.0f);

                // float _s1 = 0.0f;
                synth->source_osc_2->frequency = voice->frequency;
                float _s1 = oscillator_get_sample(synth->source_osc_2, sample_time);
                _s1 *= (voice->velocity / 127.0f);

                s += (_s + _s1) * 0.3f * envelope_get_amplitude(&voice->envelope, sample_time);
            }
            else
            {
                voice->active = false;
            }
        }

        s *= 0.3f;

        if (s < -1.0f)
            s = -1.0f;
        else if (s > 1.0f)
            s = 1.0f;

        *out++ = s;
        *out++ = s;

        sample_time += sample_increment;
        synth->stream_time += sample_increment;
    }

    return 0;
}

void synth_init(Synth* synth, u16 num_voices)
{
    assert(synth);
    assert(num_voices > 0);

    *synth = {};

    synth->num_voices = num_voices;
    synth->voices = mem_alloc_array(Voice, num_voices);

    for (u16 i = 0; i < synth->num_voices; i++)
    {
        Voice* voice = synth->voices + i;
        *voice = { };
    }

    Envelope* p_env = &synth->prefab_envelope;
    p_env->attack_time = 0.1;
    p_env->decay_time = 0.0;
    p_env->sustain_amplitude = 1.0;
    p_env->release_time = 0.1;

    oscillator_init(&synth->oscillators[0], 440, WAVE_SAW_AN, 0.6f);
    synth->source_osc_1 = &synth->oscillators[0];
    oscillator_init(&synth->oscillators[1], 220, WAVE_SINE, 0.4f);
    synth->source_osc_2 = &synth->oscillators[1];

    auto err = Pa_Initialize();
    if (err != paNoError)
    {
        fprintf(stderr, "PortAudio init failed\n");
        assert(false);
    }

    err = Pa_OpenDefaultStream(&synth->stream,
                               0, // input channels
                               2, // stereo output
                               paFloat32, // output buffer format
                               SAMPLE_RATE,
                               FRAMES_PER_BUFFER,
                               pa_callback,
                               synth);
    if (err != paNoError)
    {
        fprintf(stderr, "PortAudio opening stream failed\n");
        fprintf(stderr, "Error: %s\n", Pa_GetErrorText(err));
        assert(false);
    }

    err = Pa_StartStream(synth->stream);
    if (err != paNoError)
    {
        fprintf(stderr, "Portaudio starting stream failed\n");
        fprintf(stderr, "Error: %s\n", Pa_GetErrorText(err));
        assert(false);
    }
}

void synth_deinit(Synth* synth)
{
    assert(synth);

    auto err = Pa_StopStream(synth->stream);
    if (err != paNoError)
    {
        fprintf(stderr, "PortAudio stopping stream failed\n");
        fprintf(stderr, "Error: %s\n", Pa_GetErrorText(err));
        assert(false);
    } 

    err = Pa_CloseStream(synth->stream);
    if (err != paNoError)
    {
        fprintf(stderr, "PortAudio closing stream failed\n");
        fprintf(stderr, "Error: %s\n", Pa_GetErrorText(err));
        assert(false);
    }

    err = Pa_Terminate();
    if (err != paNoError)
    {
        fprintf(stderr, "PortAudio terminating failed\n");
        fprintf(stderr, "Error: %s\n", Pa_GetErrorText(err));
        assert(false);
    }

    mem_free(synth->voices);
}

void synth_note_down(Synth* synth, u8 note, u8 velocity)
{
    assert(synth);

    Voice* voice = nullptr;
    voice = synth_get_free_voice(synth);
    if (voice == nullptr)
    {
        for (u16 i = 0; i < synth->num_voices; i++)
        {
            Voice* v = synth->voices + i;
            if (v->note == note)
            {
                voice = v;
                printf("Recycling old voice\n");
                break;
            }
        }
    }
    if (voice)
    {
        voice->active = true;
        voice->start_time = synth->stream_time;
        voice->note = note;
        voice->velocity = velocity;
        voice->frequency = midi_get_note_frequency(note);

        Envelope* env = &voice->envelope;
        *env = synth->prefab_envelope;
        envelope_trigger_on(env, synth->stream_time);
    }
}

void synth_note_up(Synth* synth, u8 note)
{
    assert(synth);

    for (u16 i = 0; i < synth->num_voices; i++)
    {
        Voice* v = synth->voices + i;
        if (v->note == note)
        {
            v->active = false;
            envelope_trigger_off(&v->envelope, synth->stream_time);
        }
    }
}

static Voice* synth_get_free_voice(Synth* synth)
{
    assert(synth);

    Voice* result = nullptr;

    for (u16 i = 0; i < synth->num_voices; i++)
    {
        Voice* voice = synth->voices + i;
        if (voice->active == false)
        {
            result = voice;
            break;
        }
    }

    return result;
}
