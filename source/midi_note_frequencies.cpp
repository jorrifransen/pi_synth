#include "midi_note_frequencies.h"

#include <cassert>

float midi_get_note_frequency(u8 note)
{
    assert(note < 128);
    return midi_note_frequencies[note];
}
