
#include "midi.h"
#include "synth.h"

#include <stdio.h>
#include <math.h>

#include <basics/memory.h>
#include <basics/os.h>

#include <cassert>

void midi_callback(const Midi_Event& event, void* user_data)
{
    assert(user_data);
    Synth* synth = static_cast<Synth*>(user_data);

    // midi_event_print(event);

    switch (event.type)
    {
        case MIDI_EVENT_NOTE:
            if (event.note.action == MIDI_NOTE_EVENT_ON)
            {
                synth_note_down(synth, event.note.number, event.note.velocity);
            }
            else if (event.note.action == MIDI_NOTE_EVENT_OFF)
            {
                synth_note_up(synth, event.note.number);
            }
            else
            {
                assert(false);
            }
            break;

        case MIDI_EVENT_CONTROL_CHANGE:
        {
            const Midi_Control_Change_Event* cc = &event.control_change;
            if (cc->control >= 1 && cc->control <= 2)
            {
                int wave_index = ((cc->value / 127.0f) * (WAVE_COUNT - 1)) + 0.5f;
                Wave_Type wave_type = (Wave_Type)wave_index;
                synth->oscillators[cc->control - 1].wave_type = wave_type;
                printf("Osc: %d: Wave: %s\n", cc->control, get_wave_type_name(wave_type));
            }
            else if (cc->control >= 3 && cc->control <= 4)
            {
                int osc_index = cc->control -3;
                double amplitude = cc->value / 127.0;
                synth->oscillators[osc_index].amplitude = amplitude;
                printf("Osc: %d: Amp: %f\n", osc_index + 1, amplitude);
            }
            else
            {
                Envelope* p_env = &synth->prefab_envelope;
                switch (cc->control)
                {
                    case 5:
                        p_env->attack_time = (cc->value / 127.0) * 1.5;
                        break;

                    case 6:
                        p_env->decay_time = (cc->value / 127.0) * 1.5;
                        break;

                    case 7:
                        p_env->sustain_amplitude = cc->value / 127.0;
                        break;

                    case 8:
                        p_env->release_time = (cc->value / 127.0) * 1.5;
                        break;
                }
            }
            // else
            // {
            //     printf("CC: %d, %d\n", cc->control, cc->value);
            // }
        }
        break;

        deault: break;
    }
}

int main(int argc, char** argv)
{
    memory_context_init();

    int result = 0;

    // const char* midi_dev = "/dev/snd/midiC1C0";
    // if (is_file(midi_dev) == false)
    // {
    //     fprintf(stderr, "Midi device not detected\n");
    //     result = -1;
    //     goto exit_no_midi;
    // }

    Synth synth;
    Midi_Device m_dev;

    synth_init(&synth, 16);

    midi_dev_open(&m_dev, "/dev/snd/midiC1D0");
    midi_dev_set_callback(&m_dev, midi_callback, &synth);
    midi_dev_start_listening(&m_dev);

    getchar();

    midi_dev_stop_listening(&m_dev);
    midi_dev_close(&m_dev);
    synth_deinit(&synth);


 exit_no_midi:
    memory_context_deinit();

    return result;
}

